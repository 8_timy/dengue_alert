package com.example.user.denguealert.StatisticsFragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ListViewAutoScrollHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.user.denguealert.MainActivity;
import com.example.user.denguealert.R;
import com.example.user.denguealert.SQLCreator;

import org.json.JSONObject;

import java.util.ArrayList;

public class SummaryReportFragment extends Fragment {

    private ArrayList<JSONObject> symptoms;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_summary_report, null, false);

        SQLCreator sqlCreator = new SQLCreator(getActivity());
        sqlCreator.open();
        symptoms = sqlCreator.getAllSymptoms(MainActivity.user.getUserID());
        sqlCreator.close();

        ListView ls = (ListView) v.findViewById(R.id.listView);

        SummaryAdapter adapter = new SummaryAdapter(getActivity(), symptoms);
        ls.setAdapter(adapter);

        return v;
    }
}
