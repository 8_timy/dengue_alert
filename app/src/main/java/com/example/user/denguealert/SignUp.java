package com.example.user.denguealert;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.user.denguealert.Model.Accounts;

public class SignUp extends AppCompatActivity implements View.OnClickListener{

    EditText nameText, passwordText, confirmPasswordText, ageText, genderText, addressText, contactText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        nameText = (EditText) findViewById(R.id.txtUsername);
        passwordText = (EditText) findViewById(R.id.txtPassword);
        confirmPasswordText = (EditText) findViewById(R.id.txtPassConfirm);
        ageText = (EditText) findViewById(R.id.txtAge);
        genderText = (EditText) findViewById(R.id.txtGender);
        addressText = (EditText) findViewById(R.id.txtAddress);
        contactText = (EditText) findViewById(R.id.txtContactNum);

        Button signUpButton = (Button) findViewById(R.id.btnSignup);
        Button clearButton = (Button) findViewById(R.id.btnClear);
        signUpButton.setOnClickListener(this);
        clearButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSignup:
                SQLCreator sqlCreator = new SQLCreator(this);
                if(isValid()){
                    if(passwordEqual()) {
                        String name = nameText.getText().toString();
                        String password = passwordText.getText().toString();
                        int age = Integer.parseInt(ageText.getText().toString());
                        String address = addressText.getText().toString();
                        String phone = contactText.getText().toString();
                        String gender = genderText.getText().toString();

                        sqlCreator.open();
                        sqlCreator.CreateUser(name, password, age, address, phone, gender);
                        Accounts user = sqlCreator.getUser(name, password);
                        sqlCreator.close();

                        Intent intent = new Intent(SignUp.this, MainActivity.class);
                        intent.putExtra("UserInfo", user);
                        startActivity(intent);
                    }
                }
                break;
            case R.id.btnClear:
                nameText.setText("");
                passwordText.setText("");
                confirmPasswordText.setText("");
                ageText.setText("");
                addressText.setText("");
                contactText.setText("");
                genderText.setText("");

                break;
        }
    }

    private boolean isValid(){
        if(!nameText.getText().toString().isEmpty() || !passwordText.toString().isEmpty()){
            if(!confirmPasswordText.getText().toString().isEmpty() || !ageText.getText().toString().isEmpty()){
                if(!genderText.getText().toString().isEmpty() || !addressText.getText().toString().isEmpty() || !contactText.getText().toString().isEmpty()){
                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }else{
            return false;
        }
    }

    private boolean passwordEqual(){
        if(passwordText.getText().toString().equalsIgnoreCase(confirmPasswordText.getText().toString())){
            return true;
        }
        else{
            return false;
        }
    }
}

