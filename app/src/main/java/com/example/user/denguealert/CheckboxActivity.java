package com.example.user.denguealert;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;

import java.util.ArrayList;

public class CheckboxActivity extends AppCompatActivity {

    private RelativeLayout checkBoxLayout;
    private ArrayList<String> symptoms;
    private EditText temperature;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkbox);

        final String date = getIntent().getStringExtra("date");

        checkBoxLayout = (RelativeLayout) findViewById(R.id.checkBoxLayout);
        temperature = (EditText) findViewById(R.id.txtTemperature);

        Button saveButton = (Button) findViewById(R.id.btnSave);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                symptoms = getSymptoms();
                SQLCreator sqlCreator = new SQLCreator(CheckboxActivity.this);
                sqlCreator.open();
                for(String symptom : symptoms){
                    int sympID = sqlCreator.getSymptomID(symptom);
                    if(sympID == 1) {
                        sqlCreator.AddUserSymptoms(MainActivity.user.getUserID(), sympID, Double.parseDouble(temperature.getText().toString()), date);
                    }
                    else{
                        sqlCreator.AddUserSymptoms(MainActivity.user.getUserID(), sympID, 0, date);
                    }
                }
                sqlCreator.close();

                Intent intent = new Intent(CheckboxActivity.this, MainActivity.class);
                intent.putExtra("UserInfo", MainActivity.user);
                startActivity(intent);
                finish();
            }
        });

    }

    private ArrayList<String> getSymptoms(){
        ArrayList<String> symptoms = new ArrayList<>();
        for(int i = 0; i < 12; i++){
            CheckBox child = (CheckBox)checkBoxLayout.getChildAt(i);
            if(child.isChecked()){
                symptoms.add(child.getText().toString());
            }
        }

        return  symptoms;
    }

}
