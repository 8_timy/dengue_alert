package com.example.user.denguealert.SymptomsFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.user.denguealert.MainActivity;
import com.example.user.denguealert.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SymptomsFragment extends Fragment {

    private View v;
    private TextView nameText, birthday, phone, address, gender;

    public SymptomsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_profile, container, false);

        nameText = (TextView) v.findViewById(R.id.txtProfileName);
        birthday = (TextView) v.findViewById(R.id.txtAge);
        address = (TextView) v.findViewById(R.id.txtAddress);
        phone = (TextView) v.findViewById(R.id.txtPhoneNumber);
        gender = (TextView) v.findViewById(R.id.txtGender);

        nameText.setText(MainActivity.user.getName());
        birthday.setText(MainActivity.user.getAge()+"");
        address.setText(MainActivity.user.getAddress());
        phone.setText(MainActivity.user.getPhone());
        gender.setText(MainActivity.user.getGender());

        return v;
    }

}
