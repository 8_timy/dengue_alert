package com.example.user.denguealert.HomeFragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.user.denguealert.CheckboxActivity;
import com.example.user.denguealert.MainActivity;
import com.example.user.denguealert.Model.Accounts;
import com.example.user.denguealert.MonitoringCalendar;
import com.example.user.denguealert.R;
import com.example.user.denguealert.SQLCreator;

import java.lang.reflect.Array;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    private View v;
    private TextView nameText, tempText, asOfText, symptomsText, statisText;
    private Button newStatusButton;
    private ArrayList<Integer> symptoms;
    private ArrayList<String> symptomNames;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_home_new, container, false);

        symptomNames = new ArrayList<>();

        nameText = (TextView) v.findViewById(R.id.txtName);
        tempText = (TextView) v.findViewById(R.id.txtTemp);
        asOfText = (TextView) v.findViewById(R.id.txtDate);
        symptomsText = (TextView) v.findViewById(R.id.txtSymptoms);
        newStatusButton = (Button) v.findViewById(R.id.btnNewStatus);
        statisText = (TextView) v.findViewById(R.id.txtStatistics);

        newStatusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MonitoringCalendar.class));
            }
        });

        nameText.setText(MainActivity.user.getName());

        SQLCreator sqlCreator = new SQLCreator(getActivity());
        sqlCreator.open();
        Double lastTemp = sqlCreator.getLastTemperature(MainActivity.user.getUserID());
        String asOfDate = sqlCreator.getLastTemperatureDate(MainActivity.user.getUserID());
        symptoms = sqlCreator.getAllUserSymptoms(MainActivity.user.getUserID());
        for(Integer symptom : symptoms){
            String symptomName = sqlCreator.getSymptomName(symptom);
            if(!symptomNames.contains(symptomName)) {
                symptomNames.add(sqlCreator.getSymptomName(symptom));
            }
        }
        sqlCreator.close();

        tempText.setText(lastTemp+"");
        asOfText.setText(asOfDate);
        symptomsText.setText(symptomNames.toString());

        checkStatistics();

        return v;
    }

    private void checkStatistics(){
        if(symptomNames.contains("Sudden Onset of High Fever")){
            if(symptomNames.size() >= 3){
                statisText.setText("You Need to See a Doctor.");
            }
            else{
                statisText.setText("You are Okay!");
            }
        }
    }

}
