package com.example.user.denguealert;



import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.user.denguealert.Model.Accounts;

public class MainActivity extends AppCompatActivity {

    private TabHostFragment tabHostFragment;

    public static Accounts user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        user = getIntent().getParcelableExtra("UserInfo");

        tabHostFragment = new TabHostFragment();

        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_cont, tabHostFragment);
        fragmentTransaction.commit();

    }

}