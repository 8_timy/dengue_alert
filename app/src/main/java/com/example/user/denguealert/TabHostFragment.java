package com.example.user.denguealert;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.TabHost;

import com.example.user.denguealert.FaqsFragment.FaqsFragment;
import com.example.user.denguealert.HomeFragment.HomeFragment;
import com.example.user.denguealert.MonitoringFragment.MonitoringFragment;
import com.example.user.denguealert.MyFragmentPagerAdapter;
import com.example.user.denguealert.R;
import com.example.user.denguealert.ReportsFragment.ReportsFragment;
import com.example.user.denguealert.StatisticsFragment.SummaryReportFragment;
import com.example.user.denguealert.SymptomsFragment.SymptomsFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asus on 10/18/2016.
 */
public class TabHostFragment extends Fragment implements ViewPager.OnPageChangeListener, TabHost.OnTabChangeListener {

    ViewPager viewPager;
    List<Fragment> listFragments;
    TabHost tabHost;
    SymptomsFragment symptomsFragment;
    ReportsFragment reportsFragment;
    HomeFragment homeFragment;
    MonitoringFragment monitoringFragment;
    FaqsFragment faqsFragment;
    SummaryReportFragment summaryReportFragment;
    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        symptomsFragment = new SymptomsFragment();
        homeFragment = new HomeFragment();
//        monitoringFragment = new MonitoringFragment();
        faqsFragment = new FaqsFragment();
        reportsFragment = new ReportsFragment();
        summaryReportFragment = new SummaryReportFragment();

        v = inflater.inflate(R.layout.fragment_tab_host, container, false);

        initViewPager();
        initTabHost();

        return v;
    }

    private void initViewPager() {
        viewPager = (ViewPager) v.findViewById(R.id.view_pager);
        List<Fragment> listFragments = new ArrayList<Fragment>();
        listFragments.add(homeFragment);
        listFragments.add(symptomsFragment);
        listFragments.add(faqsFragment);
        listFragments.add(reportsFragment);
        listFragments.add(summaryReportFragment);


        MyFragmentPagerAdapter myFragmentPagerAdapter = new MyFragmentPagerAdapter(getChildFragmentManager(), listFragments);
        viewPager = (ViewPager) v.findViewById(R.id.view_pager);
        viewPager.setAdapter(myFragmentPagerAdapter);
        viewPager.setOnPageChangeListener(this);

    }


    private void initTabHost() {
        tabHost = (TabHost) v.findViewById(R.id.tabHost);
        tabHost.setup();

        String[] tabNames = {"Home", "Profile", "FAQs", "My Doctor", "Reports"};
        int[] icon = {R.drawable.home, R.drawable.profile, R.drawable.faq, R.drawable.doctor, R.drawable.report};

        for (int i = 0; i < tabNames.length; i++) {
            TabHost.TabSpec tabSpec;
            tabSpec = tabHost.newTabSpec(tabNames[i]);
            tabSpec.setIndicator("",getResources().getDrawable(icon[i]));
            tabSpec.setContent(new FakeContent(getActivity()));
            tabHost.addTab(tabSpec);
            tabHost.getTabWidget().getChildAt(i).getLayoutParams().width = 20;
        }

        tabHost.setOnTabChangedListener(this);
    }

    public class FakeContent implements TabHost.TabContentFactory {
        Context context;

        public FakeContent(Context mcontext) {
            context = mcontext;

        }

        public View createTabContent(String tag) {
            View fakeView = new View(context);
            fakeView.setMinimumHeight(0);
            fakeView.setMinimumWidth(0);
            return fakeView;
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int selectedItem) {

        tabHost.setCurrentTab(selectedItem);

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabChanged(String tabId) {
        int selectedItem = tabHost.getCurrentTab();
        viewPager.setCurrentItem(selectedItem);

        HorizontalScrollView hScrollView = (HorizontalScrollView) v.findViewById(R.id.h_ScrollView);
        View tabView = tabHost.getCurrentTabView();
        int scrollPos = tabView.getLeft()-(hScrollView.getWidth()-tabView.getWidth()) / 2;
        hScrollView.smoothScrollTo(scrollPos, 0);

    }
}
