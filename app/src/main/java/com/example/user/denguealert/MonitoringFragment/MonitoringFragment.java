package com.example.user.denguealert.MonitoringFragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.example.user.denguealert.CheckboxActivity;
import com.example.user.denguealert.MainActivity;
import com.example.user.denguealert.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MonitoringFragment extends Fragment {


    View v;



    public MonitoringFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_monitoring, container, false);

        final CalendarView calendar = (CalendarView) v.findViewById(R.id.calendarView);

        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                String date = dayOfMonth+"/"+month+"/"+year;
                Intent intent = new Intent(getActivity(), CheckboxActivity.class);
                intent.putExtra("date", date);
                startActivity(intent);
            }
        });

        return v;
    }

}
