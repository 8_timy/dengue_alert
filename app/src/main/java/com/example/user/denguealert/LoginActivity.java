package com.example.user.denguealert;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.user.denguealert.Model.Accounts;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText usernameText, passwordText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameText = (EditText) findViewById(R.id.txtUsername);
        passwordText = (EditText) findViewById(R.id.txtPassword);

        Button loginButton = (Button) findViewById(R.id.btnLogin);
        Button signUpButton = (Button) findViewById(R.id.btnSignUp);

        loginButton.setOnClickListener(this);
        signUpButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLogin:
                String username = usernameText.getText().toString();
                String password = passwordText.getText().toString();
                SQLCreator sqlCreator = new SQLCreator(this);
                sqlCreator.open();
                Accounts user = sqlCreator.getUser(username, password);
                sqlCreator.close();

                if(user != null){
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("UserInfo", user);
                    startActivity(intent);
                    finish();
                }
                else{
                    Toast.makeText(LoginActivity.this, "Unrecognized Account", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.btnSignUp:
                Intent intent = new Intent(LoginActivity.this, SignUp.class);
                startActivity(intent);
                break;
        }
    }
}
