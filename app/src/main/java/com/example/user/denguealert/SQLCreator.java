package com.example.user.denguealert;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.user.denguealert.Model.Accounts;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Asus on 10/17/2016.
 */
public class SQLCreator {

    private static final String DATABASE_NAME = "DBAlert.db";
    private static final int DB_VERSION = 23;

    private SQLHelper ourHelper;
    private final Context ourContext;
    private SQLiteDatabase ourDatabase;


    private static class SQLHelper extends SQLiteOpenHelper {

        public SQLHelper (Context context){
            super(context, DATABASE_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            String sqlCreateUserTable = "CREATE TABLE User (" + "UserID INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "Name TEXT, " + "Password TEXT, " + "Age INT, " + "Address TEXT, " + "Phone TEXT, " + "Gender TEXT " + ");";
            db.execSQL(sqlCreateUserTable);

            String sqlCreateStatusTable = "CREATE TABLE Status (" + "StatusID INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "UserID INT, " + "Description TEXT, " + "Date TEXT, " + "FOREIGN KEY(UserID) REFERENCES User(UserID) " + ");";
            db.execSQL(sqlCreateStatusTable);

            String sqlCreateSymptomsTable = "CREATE TABLE Symptoms (" + "SymptomID INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "Name TEXT " + ");";
            db.execSQL(sqlCreateSymptomsTable);

            String sqlCreateUserSymptomTable = "CREATE TABLE UserSymptom (" + "USID INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "UserID INT, " + "SymptomID INT, " + "Temperature REAL, " + "Date TEXT, " + "FOREIGN KEY(UserID) REFERENCES User(UserID), " + "FOREIGN KEY(SymptomID) REFERENCES Symptoms(SymptomID) " + ");";
            db.execSQL(sqlCreateUserSymptomTable);

            String addSymptom = "insert into symptoms (Name) values ('Sudden Onset of High Fever')";
            db.execSQL(addSymptom);
            addSymptom = "insert into symptoms (Name) values ('Joint Pain')";
            db.execSQL(addSymptom);
            addSymptom = "insert into symptoms (Name) values ('Pain Behind the Eyes')";
            db.execSQL(addSymptom);
            addSymptom = "insert into symptoms (Name) values ('Weakness')";
            db.execSQL(addSymptom);
            addSymptom = "insert into symptoms (Name) values ('Rashes')";
            db.execSQL(addSymptom);
            addSymptom = "insert into symptoms (Name) values ('Nosebleeding')";
            db.execSQL(addSymptom);
            addSymptom = "insert into symptoms (Name) values ('Abdominal Pain')";
            db.execSQL(addSymptom);
            addSymptom = "insert into symptoms (Name) values ('Vomiting of Coffee-colored Matter')";
            db.execSQL(addSymptom);
            addSymptom = "insert into symptoms (Name) values ('Dark-colores Stool')";
            db.execSQL(addSymptom);
            addSymptom = "insert into symptoms (Name) values ('Difficulty of Breathing')";
            db.execSQL(addSymptom);
            addSymptom = "insert into symptoms (Name) values ('Gum Bleeding')";
            db.execSQL(addSymptom);
            addSymptom = "insert into symptoms (Name) values ('Severe Headache')";
            db.execSQL(addSymptom);


            Log.i("excuted", "tableCreated");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS User");
            db.execSQL("DROP TABLE IF EXISTS Status");
            db.execSQL("DROP TABLE IF EXISTS Symptoms");
            db.execSQL("DROP TABLE IF EXISTS UserSymptom");
            onCreate(db);
        }
    }

    public SQLCreator (Context context){
        ourContext = context;
    }

    public SQLCreator open() throws SQLException{
        ourHelper = new SQLHelper(ourContext);
        ourDatabase = ourHelper.getWritableDatabase();
        return this;
    }

    public void close(){
        ourHelper.close();
    }


    public Long CreateUser(String name, String password, int age, String address, String phone, String gender){
        ContentValues cv = new ContentValues();

        cv.put("Name", name);
        cv.put("Password", password);
        cv.put("Age", age);
        cv.put("Address", address);
        cv.put("Phone", phone);
        cv.put("Gender", gender);

        return ourDatabase.insertOrThrow("User", null, cv);
    }

    public Long CreateStatus(int userID, String description, String date){
        ContentValues cv = new ContentValues();

        cv.put("UserID", userID);
        cv.put("Description", description);
        cv.put("Date", date);

        return ourDatabase.insertOrThrow("Status", null, cv);
    }

    public Long AddUserSymptoms(int userID, int symptomID, double temperature, String date){
        ContentValues cv = new ContentValues();

        cv.put("UserID", userID);
        cv.put("SymptomID", symptomID);
        cv.put("Temperature", temperature);
        cv.put("Date", date);

        return ourDatabase.insertOrThrow("UserSymptom", null, cv);
    }

    public Accounts getUser(String username, String password){
        String[] columns = new String[]{"UserID","Name", "Password", "Age","Address", "Phone", "Gender"};
        Cursor c = ourDatabase.query("User", columns, "Name" + "=" + "'" + username + "'" + " AND " + "Password" + "=" + "'" + password + "'", null, null, null, null);
//        Cursor c = ourDatabase.query("User", columns, null, null, null, null, null);
        try {
            if (c != null) {
                c.moveToFirst();
                int userID = c.getInt(0);
                String name = c.getString(1);
                String userPass = c.getString(2);
                int age = c.getInt(3);
                String address = c.getString(4);
                String phone = c.getString(5);
                String gender = c.getString(6);
                return new Accounts(userID, name, userPass, address, gender, age, phone);
            }
        }catch (Exception e){
            return null;
        }
        return null;
    }

    public String getLatestStatus(int userID){
        String[] columns = new String[]{"StatusID","UserID","Description","Date"};
        Cursor c = ourDatabase.query("Status", columns, "UserID" + "=" + userID, null, null, null, null);
        try {
            if (c != null) {
                c.moveToLast();
                String name = c.getString(2);
                return name;
            }
        }catch (Exception e){
            return null;
        }
        return null;
    }

//    public String[] getAllStatus(int userID){
//
//    }

    public double getLastTemperature(int userID){
        String[] columns = new String[]{"USID","UserID","SymptomID","Temperature","Date"};
        Cursor c = ourDatabase.query("UserSymptom", columns, "UserID" + "=" + userID + " AND " +  "SymptomID" + "=" + 1, null, null, null, null);
        try {
            if (c != null) {
                c.moveToLast();
                double temperature = c.getDouble(3);
                return temperature;
            }
        }catch (Exception e){
            return 0;
        }
        return 0;
    }

    public String getLastTemperatureDate(int userID){
        String[] columns = new String[]{"USID","UserID","SymptomID","Temperature","Date"};
        Cursor c = ourDatabase.query("UserSymptom", columns, "UserID" + "=" + userID + " AND " + "SymptomID" + "=" + 1, null, null, null, null);
        try {
            if (c != null) {
                c.moveToLast();
                String date = c.getString(4);
                return date;
            }
        }catch (Exception e){
            return null;
        }
        return null;
    }

    public ArrayList<Integer> getAllUserSymptoms(int userID){
        String[] columns = new String[]{"USID","UserID","SymptomID","Temperature","Date"};
        Cursor c = ourDatabase.query("UserSymptom", columns, "UserID" + "=" + userID, null, null, null, null);
//        Cursor c = ourDatabase.rawQuery("SELECT Symptoms.Name FROM Symptoms INNER JOIN UserSymptom ON Symptoms.SymptomID = UserSymptom.SymptomID WHERE UserSymptom.UserID = " + userID, null);
        ArrayList<Integer> symptoms = new ArrayList<>();
        try {
            if (c != null) {
                c.moveToFirst();
                do {
                    symptoms.add(c.getInt(2));
                }while (c.moveToNext());
            }
        }catch (Exception e){
            return symptoms;
        }

        return symptoms;
    }

    public ArrayList<JSONObject> getAllSymptoms(int userID){
        String[] columns = new String[]{"USID","UserID","SymptomID","Temperature","Date"};
        Cursor c = ourDatabase.query("UserSymptom", columns, "UserID" + "=" + userID, null, null, null, null);
//        Cursor c = ourDatabase.rawQuery("SELECT Symptoms.Name FROM Symptoms INNER JOIN UserSymptom ON Symptoms.SymptomID = UserSymptom.SymptomID WHERE UserSymptom.UserID = " + userID, null);
        ArrayList<JSONObject> symptoms = new ArrayList<>();
        try {
            if (c != null) {
                c.moveToFirst();
                do {
                    JSONObject object = new JSONObject();
                    object.put("name", c.getInt(2));
                    object.put("date", c.getString(4));
                    symptoms.add(object);
                }while (c.moveToNext());
            }
        }catch (Exception e){
            return symptoms;
        }

        return symptoms;
    }

    public String getSymptomName(int id){
        String[] columns = new String[]{"SymptomID","Name"};
        Cursor c = ourDatabase.query("Symptoms", columns, "SymptomID" + "=" + id, null, null, null, null);
        try {
            if (c != null) {
                c.moveToFirst();
                String name = c.getString(1);
                return name;
            }
        }catch (Exception e){
            return null;
        }
        return null;
    }

    public int getSymptomID(String name){
        String[] columns = new String[]{"SymptomID","Name"};
        Cursor c = ourDatabase.query("Symptoms", columns, "Name" + "=" + "'" + name + "'", null, null, null, null);
        try {
            if (c != null) {
                c.moveToFirst();
                int id = c.getInt(0);
                return id;
            }
        }catch (Exception e){
            return -1;
        }
        return -1;
    }
}
