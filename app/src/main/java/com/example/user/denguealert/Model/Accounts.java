package com.example.user.denguealert.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Asus on 10/17/2016.
 */
public class Accounts implements Parcelable{

    private String name;
    private String address;
    private String gender;
    private int age;
    private int userID;
    private String password;
    private String phone;

    public Accounts(){}

    public Accounts(int userID, String name, String password, String address, String gender, int age, String phone){
        this.userID = userID;
        this.name = name;
        this.password = password;
        this.address = address;
        this.gender = gender;
        this.age = age;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(userID);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(gender);
        dest.writeInt(age);
        dest.writeString(password);
        dest.writeString(phone);
    }

    protected Accounts(Parcel in){
        userID = in.readInt();
        name = in.readString();
        address = in.readString();
        age = in.readInt();
        gender = in.readString();
        password = in.readString();
        phone = in.readString();
    }

    public static Creator<Accounts> CREATOR = new Creator<Accounts>() {
        @Override
        public Accounts createFromParcel(Parcel source) {
            return new Accounts(source);
        }

        @Override
        public Accounts[] newArray(int size) {
            return new Accounts[size];
        }
    };
}
