package com.example.user.denguealert;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by User on 6/14/2016.
 */
public class MyFragmentPagerAdapter extends FragmentPagerAdapter {

    List<Fragment> listFragments;

    public MyFragmentPagerAdapter(FragmentManager fm, List<Fragment> listFragments){
        super(fm);
        this.listFragments = listFragments;
    }




    public Fragment getItem(int position) {
        return listFragments.get(position);
    }

    public int getCount() {
        return listFragments.size() ;
    }
}
