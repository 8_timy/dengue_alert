package com.example.user.denguealert.StatisticsFragment;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.user.denguealert.R;
import com.example.user.denguealert.SQLCreator;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Asus on 10/18/2016.
 */
public class SummaryAdapter extends ArrayAdapter<JSONObject> {

    private ArrayList<JSONObject> symptoms = new ArrayList<>();
    private final Activity context;

    public SummaryAdapter (Activity context, ArrayList<JSONObject> objects){
        super(context, R.layout.item, objects);
        this.context = context;
        symptoms = objects;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater= context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.item, null, false);

        JSONObject symptom = symptoms.get(position);

        TextView symptomText = (TextView) rowView.findViewById(R.id.txtSymptoms);
        TextView dateText = (TextView) rowView.findViewById(R.id.txtDate);

        SQLCreator sqlCreator = new SQLCreator(context);
        sqlCreator.open();
        symptomText.setText(sqlCreator.getSymptomName(symptom.optInt("name")));
        dateText.setText(symptom.optString("date"));

        return rowView;
    }
}
